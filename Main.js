import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Icon, TouchableHighlight, Image } from 'react-native';
import Header from './components/Header';

import Footer from './components/Footer';
import Content from './components/ContentTab';
import { StackActions, NavigationActions } from 'react-navigation';
import TabContainer from './TabContainer';



class Main extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'Logo',
            headerRight: (
                <TouchableHighlight onPress={() => navigation.navigate('Login')}>
                    <Image
                        style={styles.button}
                        source={require('./img/login.png')}
                    />
                </TouchableHighlight>
            )
        };
    };

    render() {
        return (
            <View style={styles.main}>
                <TabContainer />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    header: {
        flex: 0.6,
        backgroundColor: "blue",
        justifyContent: "center"

    },
    container: {
        flex: 5,
        backgroundColor: "red"
    },
    footer: {
        flex: 0.4,
        backgroundColor: "green"
    },
    button: {
        width: 28,
        height: 28,
        margin: 5
    }
});

export default Main;