import {createStackNavigator,createAppContainer} from 'react-navigation';
import Container from './components/ContentTab';
import Main from './Main';
import Login from './Login';


const AppNavigator = createStackNavigator({
    // For each screen that you can navigate to, create a new entry like this:
    Main: {
      // `ProfileScreen` is a React component that will be the main content of the screen.
      screen: Main,
      // When `ProfileScreen` is loaded by the StackNavigator, it will be given a `navigation` prop.
  
      // Optional: When deep linking or using react-navigation in a web app, this path is used:
      path: './Main.js',
      // The action and route params are extracted from the path.
  
      // Optional: Override the `navigationOptions` for the screen
      // navigationOptions: ({ navigation }) => ({
      //   title: `${navigation.state.params.name}'s Profile'`,
      // }),
    },
    Login : {
      screen : Login,
      path : './Login.js'
    }
  });

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;