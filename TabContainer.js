import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import ContentTab from './components/ContentTab';
import DiscoverTab from './components/DiscoverTab';


const TabNavigator = createBottomTabNavigator({
    Content: ContentTab,
    Discover: DiscoverTab
  },{
      tabBarOptions : {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
        labelStyle: {
            fontSize: 12,
        },
        style: {
            backgroundColor: '#9BD67C',
        },
        
    }
  });
  

 const tabBarOptions = () => {
    return 
};
  export default createAppContainer(TabNavigator);